<?php 
    include 'conexion.php';
    include 'menu.php';
?>

    <form action="cargar-motos.php" method="POST" enctype="multipart/form-data">
    <div class="row">
        <div class="col-sm-4">
            <div class="input-group input-group-static mb-4">
                <label for="nombre">Nombre</label>
                <input type="text" class="form-control" name="nombre">
            </div>
        </div>

        <div class="col-sm-4">
            <div class="input-group input-group-static mb-4">
                <label for="apellido">Cilindraje</label>
                <input type="number" class="form-control" name="cilindraje">
            </div>
        </div>

        <div class="col-sm-4">
            <div class="input-group input-group-static mb-4">
                <label for="apellido">Precio</label>
                <input type="number" class="form-control" name="precio">
            </div>
        </div>
        
    </div>
    <div class="row">
    <div class="col-sm-4">
            <div class="input-group input-group-static mb-4">
                <label for="imagenes">Imagenes</label>
                <input type="file" name="imagenes" id="imagenes">
            </div>
        </div>
    </div>
    <input class="btn btn-primary"type="submit" value="Enviar">
    </form>
    <script>
    $( document ).ready(function() {
    console.log( "ready!" );
    document.getElementById("moto-agregar").className += " active";
    });
  </script>
    <?php
    include 'aside.php';
    ?>